import {Status} from '../models/status.model'
export interface Week {
    name : string;
    regular : Status;
    diesel : Status;
    premium : Status;
}