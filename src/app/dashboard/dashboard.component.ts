import { Week } from './../models/week.model';
import { Status } from './../models/status.model';
import { Component, OnInit } from '@angular/core';
import { ReturnStatement } from '@angular/compiler';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  /**
   * //TODO : some properties to use in dashboard component
   */
  logo : string ='https://images.realestate.com.kh/offices/2019-07/sokimex_logo.png';
  cardTitle : string = 'Annual Month Overall Data of SUKIMEX'; 
  branchTitle : string = 'Data From each Branch';

  

  /**
   * //TODO -for generate random number of used
   * @param used -getting used number
   */
  randomUsed(used : any):any{
    return Math.floor(Math.random() * used);
  }



  /**
   * //TODO -for generate percentage of used divide available 
   * @param used -get use number
   * @param available -get available number
   */
  generatePercent(used:any, available:any):any{
    return (used * 100) / available;
  }


  regularValue = this.randomUsed(21);
  dieselValue = this.randomUsed(31);
  primuimValue = this.randomUsed(41);


  regularPercentage = (this.regularValue*100) / 20;
  dieselPercentage = (this.dieselValue*100) /30;
  primumPercentage = (this.primuimValue * 100) /40;


  /**
   * //TODO : initialize 3 array of total
   */
  status : Status[] = [
    {used : this.regularValue , available : 20},
    {used : this.dieselValue , available : 30},
    {used : this.primuimValue , available : 40},
  ];


  /**
   * //TODO get week row data
   */
  getWeekRow(){
    return {
      name : 'week',
      regular :
      {
          used : this.randomUsed(31),
          available : 30
      },
      diesel :
      {
          used : this.randomUsed(21),
          available : 20
      },
      premium : 
      {
          used : this.randomUsed(26),
          available: 25
        }
    }
  }


  constructor() { }


  ngOnInit(): void {
  }

}
