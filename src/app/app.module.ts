import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TotalComponent } from './total/total.component';
import { BranchComponent } from './branch/branch.component';
import { WeekRowComponent } from './week-row/week-row.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CheckstatusDirective } from './total/checkstatus.directive';

@NgModule({
  declarations: [
    AppComponent,
    TotalComponent,
    BranchComponent,
    WeekRowComponent,
    DashboardComponent,
    CheckstatusDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
