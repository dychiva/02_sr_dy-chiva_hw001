import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appCheckstatus]'
})
export class CheckstatusDirective {

  constructor(el:ElementRef) {
    el.nativeElement.style.type='danger';
   }

}
