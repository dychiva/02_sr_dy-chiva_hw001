import { Week } from './../models/week.model';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: '[app-week-row]',
  templateUrl: './week-row.component.html',
  styleUrls: ['./week-row.component.css']
})
export class WeekRowComponent implements OnInit {

  /**
   *   // TODO: initialize property Input week to get value from Dashboard component
   */
  @Input() weekrow : Week;

  constructor() { }

  ngOnInit(): void {
    console.log("branch"+this.weekrow);
  }

}
