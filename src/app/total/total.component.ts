import { Status } from './../models/status.model';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-total',
  templateUrl: './total.component.html',
  styleUrls: ['./total.component.css']
})
export class TotalComponent implements OnInit {

  // TODO: initialize property Input to get value from Dashboard component
  @Input() status : Status[];
  
  @Input('regularPercentage') regPercent : number;
  @Input('dieselPercentage') diePercent : number;
  @Input('primumPercentage') priPercent : number;

  isOver : boolean;

  constructor() { }

  ngOnInit(): void {
    if(this.diePercent > 70){
      this.isOver = true;
      console.log("over");
    }else{
      this.isOver = false;
      console.log("don't over");
    }
    console.log(this.regPercent);
  }

  
}
